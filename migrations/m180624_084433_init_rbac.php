<?php

use yii\db\Migration;
use app\rbac\AuthorRule;

/**
 * Class m180624_084433_init_rbac
 */
class m180624_084433_init_rbac extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
   {
        $auth = Yii::$app->authManager;
        
                // add "author" role and give this role the "createPost" permission
                $manager = $auth->createRole('manager');
                $auth->add($manager);

                $employee = $auth->createRole('employee');
                $auth->add($employee);
 
              
                
                $auth->addChild($manager, $employee);
               

              
////////////////////////////////////////////////////////////////////////////////////
                $createTask = $auth->createPermission('createTask');
                $auth->add($createTask);

                $updateTask = $auth->createPermission('updateTask');
                $auth->add($updateTask);

                $deletePost = $auth->createPermission('deleteTask');
                $auth->add($deletePost); 

                $viewTasks = $auth->createPermission('viewTasks');
                $auth->add($viewTasks);                   
       ////////////////////////////////////////////////////////////////////////////////         
                $CreateUsers = $auth->createPermission('CreateUsers');
                $auth->add($CreateUsers);

                $UpdateUser = $auth->createPermission('UpdateUser');
                $auth->add($UpdateUser);

                $updateOwnUser = $auth->createPermission('updateOwnUser');

                $rule = new \app\rbac\AuthorRule;
                $auth->add($rule);
                
                $updateOwnUser->ruleName = $rule->name;                 
                $auth->add($updateOwnUser);                  
                                                
                
                $auth->addChild($employee, $createTask);
                $auth->addChild($employee, $viewTasks);
                $auth->addChild($employee, $updateOwnUser);
                $auth->addChild($manager, $updateTask);
                $auth->addChild($manager, $deleteTask); 
                $auth->addChild($manager, $CreateUsers);
                $auth->addChild($manager, $UpdateUser);
                $auth->addChild($updateOwnUser, $UpdateUser); 
    }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180624_084433_init_rbac cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180624_084433_init_rbac cannot be reverted.\n";

        return false;
    }
    */
}
