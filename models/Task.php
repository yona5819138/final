<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use app\models\User;
use app\models\UserSearch;

/**
 * This is the model class for table "task".
 *
 * @property int $id
 * @property string $name
 * @property string $urgency
 * @property string $created_at
 * @property string $updated_at
 * @property int $crated_by
 * @property int $updated_by
 */
class Task extends \yii\db\ActiveRecord
{
      public function behaviors()
    {
        return [
            [
                'class' => BlameableBehavior::className(),
              
            ],
        ];
    }
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'task';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['created_at', 'updated_at'], 'safe'],
            [['crated_by', 'updated_by'], 'integer'],
            [['name', 'urgency'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'urgency' => 'Urgency',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'created_by' => 'Crated By',
            'updated_by' => 'Updated By',
        ];
    }
}
